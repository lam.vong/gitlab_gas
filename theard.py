import threading
import time

"""Function for incrementing counter"""
def incrementCounter(name):
    print("{} Starting".format(name))
    i = 0
    # use while loop for incrementing counter with i set to 0
    while i < 10:
        # Sleep for 1 second
        time.sleep(1)
        i += 1
        # print counter value
        print ("{} counter value - {}".format(name, i))
    print("{} Exiting".format(name))

"""Function for decrementing counter"""
def decrementCounter(name):
    print("{} starting".format(name))
    i = 10
    # use while loop for decrementing counter with i set to 10
    while i > 0:
        # Sleep for 1 second
        time.sleep(1)
        i -= 1
        # print counter value
        print ("{} counter value - {}".format(name, i))
    print("{} exiting".format(name))

# create two threads and assign to functions
t1 = threading.Thread(name='Thread 1', target=incrementCounter, args=["Thread 1"])
t2 = threading.Thread(name='Thread 2', target=decrementCounter, args=["Thread 2"])

# start and join threads
t1.start()
t2.start()
t1.join()
t2.join()
